import os
import sys
from Cajero import Cajero
from Cliente import Cliente
from Funciones import *
"""Se brinda la bienvenida"""
print('')
print('BIENVENIDO A BANCO DIGITALY')
print('')
"""se declaran  variables"""
intentos = 0           se declaran  variables
opcion = 0
clave_introducida = None
max_intentos = 3

cajero = Cajero()
cliente = Cliente();

while intentos < max_intentos:

    if clave_introducida == None :

        clave_introducida = input('POR FAVOR INGRESE SU CLAVE: ')
        print('')
    if clave_introducida == str( cliente.clave_cajero ):

"""impresion de menu  principal"""
        print('POR FAVOR SELECIONE UN OPCION:')
        print('')
        print('1.CONSULTAR ')
        print('2.RETIRO')
        print('3.CANCELAR')
        print('')
        opcion = input()

"""salida   de opcion de menu principal"""

        if opcion == str( cajero.opciones['cancelar'] ):
            break
        elif opcion == str( cajero.opciones['consulta'] ):
            consultar( cliente )
            break
        elif opcion == str( cajero.opciones['retiro'] ):
            retirar( cajero, cliente );
            print(' Saldo Disponible: ${0}'.format(cliente.saldo))
            break
        else:
            """ comenterio en caso de un contraseña incorrecta"""
            print(' Intentalo nuevamente')
            print('')
    else:

        if intentos + 1 == max_intentos:
            print('Su clave es incorrecta')
            print('')
        else:
            print(' Su clave es incorrecta. de equivocarse más de', (max_intentos - (intentos + 1)),'veces, su tarjeta será bloqueada')
        clave_introducida = None
    intentos += 1;
   
print('')
"""despedida"""
print('Gracias por ser Cliente de banco digitaly')

